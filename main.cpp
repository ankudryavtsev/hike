#include <iostream>
#include <string>
#include <climits>
#include <vector>
#include <iomanip>      // std::setw
#include <fstream>

using namespace std;

struct edge {
	int a, b, cost;
};

int n;//Количество вершин
int m;//Количество ребер
int v=0;//Исходная вершина
int len;//Длина исходной строки
vector<edge> e;
void solve();
void fromString(string input, int len);
void addEdge(int a, int b, int cost);
ofstream outputStream("output_bridge.txt");

int main()
{
    string input;
    ifstream inputStream("input_bridge.txt");
    inputStream >> input;
    inputStream.close();
    len=input.length();
    fromString(input, len);
    solve();
    outputStream.close();
    return 0;
}

void addEdge(int a, int b, int cost)
{
    edge newEdge = {a, b, cost};
    e.push_back(newEdge);
    edge newEdge2 = {b, a, cost};
    e.push_back(newEdge2);
}

void fromString(string input, int len)
{
    n = (len+1)*2;
    m = (len*3+1)*2;
    for(int i=0;i<len;i++)
    {
        if(input[i]=='L')
        {
            addEdge(i, i+1, 1);
            addEdge(i+len+1, i+len+2, 0);
        }
        else if(input[i]=='R')
        {
            addEdge(i, i+1, 0);
            addEdge(i+len+1, i+len+2, 1);
        }
        else
        {
            addEdge(i, i+1, 1);
            addEdge(i+len+1, i+len+2, 1);
        }
        addEdge(i, i+len+1, 1);
    }
    addEdge(len, len*2+1, 1);
}

void solve() {
	vector<int> d (n, INT_MAX);
	d[v] = 0;
	for (;;) {
		bool any = false;
		for (int j=0; j<m; ++j)
			if (d[e[j].a] < INT_MAX)
				if (d[e[j].b] > d[e[j].a] + e[j].cost) {
					d[e[j].b] = d[e[j].a] + e[j].cost;
					any = true;
				}
		if (!any)  break;
	}
	outputStream << d[2*len+1];
}
